## NFS container

To get NFS container running we need NFS modules installed on the host machine:

```
docker-machine ssh confluence 'apt-get update & apt-get install -y nfs-kernel-server nfs-common & systemctl stop nfs-server & systemctl disable nfs-server & systemctl stop rpcbind.service & systemctl disable rpcbind.service & reboot'
```

Hetzner server has AppArmor installed and enabled by default. To get it play nicely with
NFS server we need a special profile (file `erichough-nfs`). Copy the file
`nfs/erichough-nfs` to `/root/erichough-nfs` then execute

```
docker-machine ssh confluence 'apt-get install -y apparmor-utils lxc & apparmor_parser -r -W /root/erichough-nfs & modprobe nfs & modprobe nfsd'
```

These commands should be executed on every docker host reboot:

```
apparmor_parser -r -W /root/erichough-nfs
modprobe nfs
modprobe nfsd
```

The shared_home should be writable by any user otherwise NFS client will get a permission error:

```
docker-machine ssh confluence 'mkdir /var/docker/nfs & chmod 0777 /var/docker/nfs'
```
